#!/usr/bin/env python
import os, sys
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from importlib import import_module
from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
##soon to be deprecated
from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetmetUncertainties import *
##new way of using jme uncertainty
from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetmetHelperRun2 import *



class exampleProducer(Module):
    def __init__(self, jetSelection):
        self.jetSel = jetSelection
        pass

    def beginJob(self):
        pass

    def endJob(self):
        pass

    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        self.out.branch("n_nano_mu"      , "I");
        self.out.branch("n_nano_el"      , "I");
        self.out.branch("n_nano_lep"     , "I");
 

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass


    def analyze(self, event):
        """process event, return True (go to next module) or False (fail, go to next event)"""

        muons     = Collection(event, "Muon")
        electrons = Collection(event, "Electron")

        ## leptons
        self.out.fillBranch("n_nano_mu"  , len(muons))
        self.out.fillBranch("n_nano_el"  , len(electrons))
        self.out.fillBranch("n_nano_lep" , (len(muons)+len(electrons)))
        
        return True


dasAnalysis = lambda : exampleProducer(jetSelection= lambda j : j.pt > 10)
